\documentclass[final]{beamer}
%% Possible paper sizes: a0, a0b, a1, a2, a3, a4.
%% Possible orientations: portrait, landscape
%% Font sizes can be changed using the scale option.
\usepackage[size=a0,orientation=portrait,scale=1.2]{beamerposter}
\usepackage[backend=biber,style=numeric,sorting=none]{biblatex}
\addbibresource{HKvsLieb.bib}
\usepackage{mathtools}

\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\blue}[1]{\textcolor{blue}{#1}}
\newcommand{\green}[1]{\textcolor{green}{#1}}
\newcommand{\purple}[1]{\textcolor{purple}{#1}}
\newcommand{\hH}{\Hat{H}} 
\newcommand{\hT}{\Hat{T}} 
\newcommand{\hV}{\Hat{V}} 
\newcommand{\hn}{\Hat{n}}
\newcommand{\cre}[1]{a_{#1}^\dagger}
\newcommand{\ani}[1]{a_{#1}} 

\newcommand{\br}{\boldsymbol{r}} 

\newcommand{\up}{\uparrow} 
\newcommand{\dw}{\downarrow} 
\newcommand{\Dv}{\Delta v} 
\newcommand{\Dn}{\Delta n} 
\newcommand{\ie}{\textit{i.e.}\xspace}

\AtEveryBibitem{%
\ifentrytype{article}{
    \clearfield{url}%
    \clearfield{urlyear}%
    \clearfield{doi}%
    \clearfield{issn}%
    \clearfield{month}%
    \clearfield{day}%
    \clearfield{title}%
}{}
}

\DeclareFieldFormat[article]{pages}{#1}

\renewbibmacro{in:}{%
  \ifentrytype{article}{}{\printtext{\bibstring{in}\intitlepunct}}}

\usetheme{gemini}
\usecolortheme{gemini}
\useinnertheme{rectangles}

% ====================
% Packages
% ====================

\usepackage[utf8]{inputenc}
\usepackage{graphicx,dcolumn,xcolor,microtype,multirow,amscd,amsmath,amssymb,amsfonts,physics,wrapfig,tikz,siunitx,bm}
\usepackage{booktabs}
\usepackage{tikz}
\usepackage{pgfplots}

\newfontfamily\qp{Times New Roman}[
  % the font has no small caps, so we use another one for them
  SmallCapsFont=TeX Gyre Termes,
  SmallCapsFeatures={Letters=SmallCaps}
]


% ====================
% Lengths
% ====================

% If you have N columns, choose \sepwidth and \colwidth such that
% (N+1)*\sepwidth + N*\colwidth = \paperwidth
\newlength{\sepwidth}
\newlength{\colwidth}
\setlength{\sepwidth}{0.03\paperwidth}
\setlength{\colwidth}{0.45\paperwidth}

\newcommand{\separatorcolumn}{\begin{column}{\sepwidth}\end{column}}


% ====================
% Logo (optional)
% ====================

% LaTeX logo taken from https://commons.wikimedia.org/wiki/File:LaTeX_logo.svg
% use this to include logos on the left and/or right side of the header:
\logoright{\vspace{6cm} \includegraphics[width=10cm]{fig/Fermi.jpg}}
\logoleft{\vspace{-6cm} \includegraphics[width=10cm]{fig/ERC.jpg}}% \hspace{1cm} \includegraphics[height=6cm]{fig/CNRS.png} }
\addtobeamertemplate{headline}{} 
{
\begin{tikzpicture}[remember picture,overlay] 
\node [shift={(7.5 cm,-3.5cm)}] at (current page.north west) {\includegraphics[height=6cm]{fig/CNRS.png}}; 
\node [shift={(-7.25 cm,-4.5cm)}] at (current page.north east) {\includegraphics[width=10cm]{fig/LCPQ.jpg}}; 
\end{tikzpicture} 
}

% ====================
% Footer (optional)
% ====================

\footercontent{
	\href{mailto:sgiarrusso@irsamc.ups-tlse.fr}{\texttt{sgiarrusso@irsamc.ups-tlse.fr}}
	\hfill
	Sara Giarrusso and Pierre-Fran\c cois Loos, arXiv preprint arXiv:2303.15084
	\hfill
	\href{mailto:loos@irsamc.ups-tlse.fr}{\texttt{loos@irsamc.ups-tlse.fr}
	}
}
% (can be left out to remove footer)

% ====================
% My own customization
% - BibLaTeX
% - Boxes with tcolorbox
% - User-defined commands
% ====================
%\input{custom-defs.tex}

%% Reference Sources
%\addbibresource{refs.bib}
%\renewcommand{\pgfuseimage}[1]{\includegraphics[scale=2.0]{#1}}

\newcommand{\LCPQ}{Laboratoire de Chimie et Physique Quantiques (UMR 5626), Universit\'e de Toulouse, CNRS, UPS, France}

\title{Exact Excited-State Functionals of the Asymmetric Hubbard Dimer}

\author{\underline{Sara Giarrusso},\inst{1}, Pierre-Fran\c cois Loos,\inst{1}}
\institute[shortinst]{\inst{1} \LCPQ}

\begin{document}

\begin{frame}[t]

%%%%%%%%%%%%%%%%
%%% Abstract %%%
%%%%%%%%%%%%%%%%
\begin{alertblock}{Abstract}
We derive, for the case of the asymmetric Hubbard dimer at half-filling, the exact functional associated with each singlet ground and excited state, using both Levy's constrained search and Lieb's convex formulation.
While the ground-state functional is, as commonly known, a convex function with respect to the density (or, more precisely, the site occupation), the functional associated with the (highest) doubly-excited state is found to be concave.
Also, we find that, because the density of the first excited state is non-invertible, its ``functional'' is a partial, multi-valued function composed of one concave and one convex branch that correspond to two separate sets of values of the external potential. 
These findings offer insight into the challenges of developing state-specific excited-state density functionals for general applications in electronic structure theory.
\end{alertblock}
%%%%%%%%%%%%%%%%

\vspace{-1.3cm}
\begin{columns}[t]
\separatorcolumn

\begin{column}{\colwidth}

    %\vspace{-2cm}
     %%%%%%%%%%%%%%%%%%%%%%
    \begin{block}{Theoretical background}
The variational principle can be reformulated in terms of the Hohenberg-Kohn functional as
\begin{equation}
	\label{eq:HKvar}
	E_0[v] = \min_{\rho}\qty{ F[\rho] + \int v(\br) \rho(\br) d\br } \Leftrightarrow  \fdv{F[\rho(\br)]}{\rho(\br)} = - v(\br) 
\end{equation}
$ F[\rho]$ is the ``Fenchel conjugate" of $E_0[v]$ and can be obtained from the Lieb variational principle (or convex formulation) as: 
\begin{equation}
	\label{eq:Lvar}
	F[\rho] = \max_{v}\qty{ E[v] - \int v(\br) \rho(\br) d\br } \Leftrightarrow \fdv{E[v(\br)]}{v(\br)} = \rho(\br) 
\end{equation}
Levy's constrained search requires $\rho$ to be only $N$-representable rather than $v$-representable, defining the Levy functional as
\begin{equation}
\label{eq:FLL}
	F[\rho] 
	= \min_{\Psi \leadsto \rho} \mel{\Psi}{\hT + \hV_{ee}}{\Psi}
\end{equation}
    \end{block}
    %%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%%%%%%%%
    %\vspace{-1.cm}
    \begin{block}{The model}
The Hamiltonian of the asymmetric Hubbard dimer is
\begin{equation}
    \hH = 
    - t \sum_{\sigma=\up,\dw} \qty( \cre{0\sigma} \ani{1\sigma} + \text{h.c.} )
    + U \sum_{i=0}^{1} \hat{n}_{i\up} \hat{n}_{i\dw} 
	+  \Dv \frac{\hn_{1} - \hn_{0}}{2}
\end{equation}
A generic singlet wave function can be written as 
\begin{equation}
	\ket{\Psi} = x \ket{0_\up0_\dw} + y \frac{\ket{0_\up1_\dw} - \ket{0_\dw1_\up}}{\sqrt{2}} + z \ket{1_\up1_\dw}
\end{equation}
with $-1 \le x,y,z \le 1$ and the normalization condition 
\begin{equation}
\label{eq:normalization}
	x^2 + y^2 + z^2 = 1
\end{equation}
    \end{block}
    %%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%%%%%%%%
      %  \vspace{-0.5cm}
    \begin{block}{Ground and excited-state energies and densities}
   
    The total energy, $E$, is given by the sum of
    \begin{subequations}
\begin{align}
	\label{eq:T}
	&T  = - 2\sqrt{2} t y \qty(x + z)
	\\
	\label{eq:Vee}
	&V_{ee}  = U \qty(x^2 + z^2)
	\\
	\label{eq:V}
	&V  = \rho \cdot \Dv \quad \text{with} \quad \rho=\mel{\Psi}{\frac{\hn_{1} - \hn_{0}}{2}}{\Psi} =  (z^2 - x^2)
\end{align}
\end{subequations}
        \includegraphics[scale=1.7]{fig/fig1} 
        
        \begin{columns}[T]
    \begin{column}{.45\textwidth}
        \includegraphics[scale=1.4]{fig/fig2} 
        \end{column}
         \begin{column}{.45\textwidth}
         In agreement with Eq.~\eqref{eq:Lvar}, one finds
         \begin{equation}
         \dv{E_0(\Dv)}{\Dv} =2\, \rho_0(\Dv)
         \end{equation}
         However, the same holds for the excited states	
       \begin{subequations}
\begin{align}
        & \dv{E_1(\Dv)}{\Dv} =2\, \rho_1(\Dv)\\
        & \dv{E_2(\Dv)}{\Dv} =2\, \rho_2(\Dv)
         \end{align}
         \end{subequations}
        \end{column}
         \begin{column}{.1\textwidth}
         
          \end{column}
         \end{columns}
    \end{block}
    %%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%%%%%%%%
        %\vspace{-0.5cm}
    \begin{block}{Levy's constrained search}
    \begin{columns}[T]
         \begin{column}{.35\textwidth}
         \hspace{-3.5cm}
        $\phantom{0\,\,}$\includegraphics[scale=1.2]{fig/fig5} 
        \end{column}
         \begin{column}{.5\textwidth}
         \vspace{1cm}
        Substituting $x$ and $z$, we obtain the four-branch function
        \hspace{-3cm}
{\small{\begin{align}
	\label{eq:fpm}
	f_{\pm\pm}(\rho,y) 
	= & - 2 t y \qty (\pm\sqrt{1 - y^2 - \rho} 
	\pm \sqrt{1 - y^2 + \rho}) \notag
	\\
	&+ U \qty(1 - y^2)
\end{align}} }

Rather than only minimizing $f_{\pm\pm}(\rho,y) $
for a given $\rho$, we seek \textit{all}  its stationary points with respect to $y$:
\begin{equation}
	\pdv{f_{\pm\pm}(\rho,y)}{y} = 0
\end{equation}
        \end{column}
         \begin{column}{.1\textwidth}
          \end{column}
         \end{columns}
    \end{block}
    %%%%%%%%%%%%%%%%%%%%%%

\end{column}

\separatorcolumn

\begin{column}{\colwidth}

    \vspace{3cm}
    %               
    %%%%%%%%%%%%%%%%%%%%%%
%    \begin{block}{}
    
    \includegraphics[scale=1.35]{fig/fig3} 
    $f_{--}(\rho,y)$ (red) and $f_{++}(\rho,y)$ (green) have each one stationary point for $y \geq 0$, while $f_{+-}(\rho,y)$ (yellow) has two stationary points that disappear for $\rho > \rho_c$.
 %   \end{block}
\vspace{0.5cm}
    %%%%%%%%%%%%%%%%%%%%%%
    \begin{block}{Ground and excited-state ``universal" function(al)s}
     %
     \begin{columns}[T]
         \begin{column}{.45\textwidth}
         \includegraphics[scale=1.5]{fig/fig4} \\
         \includegraphics[scale=1.5]{fig/Vvsrho} 
         \end{column}
         \begin{column}{.45\textwidth}
         \vspace{0.5cm}
             \begin{subequations}
         \begin{align}
        & F_0(\rho)=f_{++}(\rho,y_0)\\
        & F_2(\rho)=f_{--}(\rho,y_2)\\
        &  \begin{rcases}    F_1^{\cup}(\rho) = f_{+-}(\rho,y_1^{\cup}))\\
          F_1^{\cap}(\rho) = f_{+-}(\rho,y_1^{\cap}
      \end{rcases} \quad \rho < \rho_\text{c}
         \end{align}
         \end{subequations}
 
\vspace{1.5cm}
%\dv{F_2(\rho)}{\rho} = -\Dv_2 (\rho)
 In agreement with Eq.~\eqref{eq:HKvar} and beyond the ground state, one finds
    \begin{subequations}
\begin{align}
        & \dv{F_0(\rho)}{\rho} = -\Dv_0 (\rho)\\
        & \dv{F_2(\rho)}{\rho} = -\Dv_2 (\rho)\\
        &  \begin{rcases}   \dv{F_1^{\cup}(\rho)}{\rho} = - \Dv_1^{\cup}(\rho)\\
        \dv{F_1^{\cap}(\rho)}{\rho} = -\Dv_1^{\cap}(\rho) \end{rcases} \quad \rho < \rho_\text{c}
         \end{align}
         \end{subequations}
         \end{column}
         \end{columns}
    \end{block}
    %%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%%%%%%%%
    \begin{block}{Lieb's convex formulation}
    \vspace{0.5cm}
      \begin{columns}
\begin{column}{.45\textwidth}
         \includegraphics[scale=1.5]{fig/fig6} 
          \end{column}
          \begin{column}{.5\textwidth}
\begin{equation}
	f_{n}(\rho,\Dv) = E_n - \Dv \rho
\end{equation}
Rather than only maximizing $ f_{n}(\rho,\Dv) $ for a given $\rho$, we seek \textit{all} its stationary points with respect to $\Dv$ for each $n$ value, 
\begin{equation}
	\pdv{f_{n}(\rho,\Dv)}{\Dv} = 0
\end{equation}         
           \end{column}
         \end{columns}
    \end{block}
    \vspace{-1.3cm}
    %%%%%%%%%%%%%%%%%%%%%%
        \begin{block}{Conclusions}
        While the ground-state functional is well-known to be a convex function with respect to the difference in site occupations, the functional associated with the highest doubly-excited state is found to be concave. 
Additionally, and more importantly, we discovered that the ``functional'' for the first excited state is a partial, multi-valued function of the density that is constructed from one concave and one convex branch associated with two separate sets of values of the external potential. 
Finally, we find that Levy's constrained search and Lieb's convex formulation are entirely consistent for all the states of the model, producing the same landscape of state-specific functionals.
  \end{block}
    %%%%%%%%%%%%%%%%%%%%%%%
      \vspace{-0.4cm}
    %%% Acknowledgement %%%
    %%%%%%%%%%%%%%%%%%%%%%%
    \begin{block}{Acknowledgement}
        \textit{This project has received funding from the European Research Council (ERC) 
        under the European Union's Horizon 2020 research and innovation programme,
        grant agreement No.~863481.}
    \end{block}
    %%%%%%%%%%%%%%%%%%%%%%%
 \vspace{-1.cm}
    %%%%%%%%%%%%%%%%%%
    %%% References %%%
    %%%%%%%%%%%%%%%%%%
    \begin{block}{References}

        %\printbibliography
        {\footnotesize{
        \parbox[b]{\hsize}{[1] P. Hohenberg, and W. Kohn, \textit{Inhomogeneous electron gas}, Phys. rev. 136.3B (1964): B864.}\\

\parbox[b]{\hsize}{[2] M. Levy, \textit{Universal variational functionals of electron densities, first-order density matrices, and natural spin-orbitals and solution of the v-representability problem} Proc. Nat. Ac. Sci 76.12 (1979): 6062-6065.}\\
        
 \parbox[b]{\hsize}{[3] E. H. Lieb, \textit{Density functionals for Coulomb systems}  Int. J. Quantum Chem. \textbf{24}, 243 (1983).}\\
        
        \parbox[b]{\hsize}{[4]  D. J. Carrascal, J. Ferrer, J. C. Smith, and K. Burke, \textit{The Hubbard dimer: a density functional case study of a many-body problem}, J.Phys. Condens. Matter \textbf{27}, 393001 (2015).}\\
        
       \parbox[b]{\hsize}{[5]     J. P. Perdew, and M. Levy, \textit{Extrema of the density functional for the energy: Excited states from the ground-state theory}, Phys. Rev. B 31.10 (1985): 6264. \\
     }
      }}

    \end{block}
    %%%%%%%%%%%%%%%%%%

\end{column}

\separatorcolumn
\end{columns}

\end{frame}

\end{document}
